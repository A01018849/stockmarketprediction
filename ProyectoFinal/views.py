from __future__ import division

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
import requests
from django.db.models import Count
from django.shortcuts import render_to_response, redirect, render
from django.template import RequestContext
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate as djangoAuthenticate,login as djangoLogin, logout

from ProyectoFinal.modelTrainer import ModelTrainer
from ProyectoFinal.models import FavoriteCompany, Company, Record, ThetaValue, UtilityValues


@csrf_protect
def loginPage(request):
    return render_to_response('ProyectoFinal/Authentication/login.html', RequestContext(request))

@csrf_protect
def authenticate(request):
    login_name = request.POST['login_name']
    password = request.POST['password']
    user = djangoAuthenticate(username=str(login_name),password=str(password))

    if user != None:
        djangoLogin(request,user)
        return redirect('mainMenu')

    return render(request, 'ProyectoFinal/Authentication/login.html', {
        'error_message': "Login incorrecto"
    })

def logoutPage(request):
    logout(request)

    return render(request, 'ProyectoFinal/Authentication/login.html', {
        'error_message': "Sesion terminada"
    })

@login_required
def mainMenu(request):
    return render(request,'ProyectoFinal/menu.html', {
        'loggedUser': request.user.username
    })

def createUserForm(request):
    return render(request,"ProyectoFinal/Authentication/createUser.html")

def createUser(request):
    username = request.POST['username']
    first_name = request.POST['first_name']
    last_name = request.POST['last_name']
    email = request.POST['email']
    password = request.POST['password']
    password_conf = request.POST['password_conf']

    if User.objects.filter(username=username).exists():
        return render(request, 'ProyectoFinal/Authentication/createUser.html', {
            'error_message': "Nombre de usuario existente"
        })
    if User.objects.filter(email=email).exists():
        return render(request, 'ProyectoFinal/Authentication/createUser.html', {
            'error_message': "Ya existe un usuario con ese correo."
        })
    if password != password_conf:
        return render(request, 'ProyectoFinal/Authentication/createUser.html', {
            'error_message': "Las contrasenas no coinciden"
        })

    user = User()
    user.username = username
    user.first_name = first_name
    user.last_name = last_name
    user.email = email
    user.set_password(password)
    user.save()

    return render(request, 'ProyectoFinal/Authentication/login.html', {
        'error_message': "Usuario creado con exito"
    })


@login_required
def accountMenu(request):
    return render(request,"ProyectoFinal/Profile/profileInformation.html", {
        "user": request.user
    })

@login_required
def userFavorites(request):
    favorites = FavoriteCompany.objects.filter(user_id=request.user.id)
    return render(request,"ProyectoFinal/Profile/userFavorites.html", {
        "favorites": favorites
    })

@login_required
def companyConsultMenu(request):

    general_favorite_count =FavoriteCompany.objects.values("company_id").annotate(favorite_count=Count("company_id")).order_by("-favorite_count")[:10]
    general_favorite_companies = []

    search_string = ""
    search_results = []
    if "search_string" in request.POST:
        if request.POST["search_string"] != "":
            search_string = request.POST["search_string"]
            result_companies = Company.objects.filter(name__icontains=search_string)
            for company in result_companies:
                search_results.append(company)
            result_companies = Company.objects.filter(initials__icontains=search_string)
            for company in result_companies:
                if company not in search_results:
                    search_results.append(company)

    for company in general_favorite_count:
        general_favorite_companies.append(Company.objects.filter(id = company["company_id"]).first())
    return render(request,"ProyectoFinal/CompanyConsult/newCompanyConsult.html",{
        "popularCompanies": general_favorite_companies,
        "searchString": search_string,
        "searchResults": search_results
    })

@login_required
def companyInfo(request,company_id):
    company = Company.objects.filter(id=company_id).first()

    if ThetaValue.objects.filter(company_id = company.id).exists():
        theta_value = ThetaValue.objects.filter(company_id = company.id).order_by('-datetime').first()
    else:
        theta_value = ModelTrainer.trainModelForSingleCompany(ModelTrainer(),company.id)

    todays_response = requests.get('http://finance.yahoo.com/d/quotes.csv?s='+ company.initials + '&f=ghovp')._content.replace('\n','')
    todays_values = todays_response.split(',')

    utility_values = UtilityValues()
    utility_values.calculate(company.id)

    normalized_low = (float(todays_values[0]) - utility_values.low.avg) / (utility_values.low.max - utility_values.low.min)
    normalized_high = (float(todays_values[1]) - utility_values.high.avg) / (utility_values.high.max - utility_values.high.min)
    normalized_open = (float(todays_values[2]) - utility_values.open.avg) / (utility_values.open.max - utility_values.open.min)
    normalized_volume = (float(todays_values[3]) - utility_values.volume.avg) / (utility_values.volume.max - utility_values.volume.min)
    normalized_close = (float(todays_values[4]) - utility_values.close.avg) / (utility_values.close.max - utility_values.close.min)

    normalized_prediction = (theta_value.low_theta * normalized_low) + (theta_value.high_theta * normalized_high) + (theta_value.open_theta * normalized_open) + (theta_value.volume_theta * normalized_volume) + (theta_value.close_theta * normalized_close)
    prediction = (normalized_prediction * (utility_values.close.max - utility_values.close.min)) + utility_values.close.avg
    #prediction = (theta_value.low_theta * float(todays_values[0])) + (theta_value.high_theta * float(todays_values[1])) + (theta_value.open_theta * float(todays_values[2])) + (theta_value.volume_theta * float(todays_values[3])) + (theta_value.close_theta * float(todays_values[4]))
    latest_records = Record.objects.filter(company_id = company_id).order_by("-date")[:10]



    return render(request, "ProyectoFinal/CompanyConsult/companyInfo.html", {
        "company": company,
        "low": todays_values[0],
        "high": todays_values[1],
        "open": todays_values[2],
        "volume": todays_values[3],
        "close": todays_values[4],
        "prediction": prediction,
        "record1":latest_records[0],
        "record2": latest_records[1],
        "record3": latest_records[2],
        "record4": latest_records[3],
        "record5": latest_records[4],
        "record6": latest_records[5],
        "record7": latest_records[6],
        "record8": latest_records[7],
        "record9": latest_records[8],
        "record10": latest_records[9]

    })

@login_required
def addToFavorites(request,company_id):
    user_id = request.user.id
    company = Company.objects.filter(id=company_id).first()
    if FavoriteCompany.objects.filter(company_id=company_id,user_id=user_id).exists():
        return render(request, "ProyectoFinal/CompanyConsult/companyInfo.html", {
            "company": company,
            "error_message":company.name + " ya se encontraba en tus favoritos."
        })
    else:
        favorite = FavoriteCompany()
        favorite.user_id = user_id
        favorite.company_id = company_id
        favorite.save()
        return render(request, "ProyectoFinal/CompanyConsult/companyInfo.html", {
            "company": company,
            "error_message":company.name + " agregado a tus favoritos."
        })