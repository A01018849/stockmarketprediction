from __future__ import division
from django.contrib.auth.models import User
from django.db import models

class Company(models.Model):
    name = models.CharField(max_length=100)
    initials = models.CharField(max_length=10)

class FavoriteCompany(models.Model):
    user = models.ForeignKey(User)
    company = models.ForeignKey(Company)

class Record(models.Model):
    company = models.ForeignKey(Company)
    date = models.DateField()
    open = models.FloatField()
    high = models.FloatField()
    low = models.FloatField()
    close = models.FloatField()
    volume = models.FloatField()
    adj_close = models.FloatField()

class ThetaValue(models.Model):
    company = models.ForeignKey(Company)
    datetime = models.DateTimeField()
    open_theta = models.FloatField()
    high_theta = models.FloatField()
    low_theta = models.FloatField()
    close_theta = models.FloatField()
    volume_theta = models.FloatField()

class UtilityValueGroup(object):
    def __init__(self):
        self.min = 999999
        self.max = -999999
        self.avg = 0

class UtilityValues(object):
    def __init__(self):
        self.low = UtilityValueGroup()
        self.high = UtilityValueGroup()
        self.volume = UtilityValueGroup()
        self.open = UtilityValueGroup()
        self.close = UtilityValueGroup()
        self.adj_close = UtilityValueGroup()

    def calculate(self,company_id):
        all_records = Record.objects.filter(company_id=company_id)
        for record in all_records:

            self.low.avg += record.low
            self.high.avg += record.high
            self.volume.avg += record.volume
            self.open.avg += record.open
            self.close.avg += record.close
            self.adj_close.avg += record.adj_close

            if record.low < self.low.min:
                self.low.min = record.low
            if record.low > self.low.max:
                self.low.max = record.low

            if record.high < self.high.min:
                self.high.min = record.high
            if record.high > self.high.max:
                self.high.max = record.high

            if record.volume < self.volume.min:
                self.volume.min = record.volume
            if record.volume > self.volume.max:
                self.volume.max = record.volume

            if record.open < self.open.min:
                self.open.min = record.open
            if record.open > self.open.max:
                self.open.max = record.open

            if record.close < self.close.min:
                self.close.min = record.close
            if record.close > self.close.max:
                self.close.max = record.close

            if record.adj_close < self.adj_close.min:
                self.adj_close.min = record.adj_close
            if record.adj_close > self.adj_close.max:
                self.adj_close.max = record.adj_close

        self.low.avg /= all_records.count()
        self.high.avg /= all_records.count()
        self.volume.avg /= all_records.count()
        self.open.avg /= all_records.count()
        self.close.avg /= all_records.count()
        self.adj_close.avg /= all_records.count()

