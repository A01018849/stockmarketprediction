"""ProyectoFinal URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.contrib import admin

from ProyectoFinal import views

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$',views.mainMenu,name="mainMenu"),
    url(r'^accounts/login/', views.loginPage, name='login'),
    url(r'^accounts/logout/', views.logoutPage, name='logout'),
    url(r'^menu/',views.mainMenu,name="mainMenu"),
    url(r'^authenticate/',views.authenticate,name="authenticate"),
    url(r'^createUserForm/',views.createUserForm,name="createUserForm"),
    url(r'^createUser/',views.createUser,name="createUser"),
    url(r'^accountMenu/',views.accountMenu,name="accountMenu"),
    url(r'^companyConsultMenu/',views.companyConsultMenu,name="companyConsultMenu"),
    url(r'^userFavorites/',views.userFavorites,name="userFavorites"),
    url(r'^companyInfo/(?P<company_id>\d+)/$',views.companyInfo,name="companyInfo"),
    url(r'^addToFavorites/(?P<company_id>\d+)/$',views.addToFavorites,name="addToFavorites"),

]
