from __future__ import division

import datetime

from ProyectoFinal.models import Record, ThetaValue, UtilityValues


class ModelTrainer:
    def trainModelForAllCompanies(self):
        size = Record.objects.count()
        allRecords = Record.objects.all()
        tetas = [0,0,0,0,0]
        cont = 0
        alpha = [1000,1000,1000,1000,1000]
        for i in range(40):
            for record in allRecords:
                cont = cont+1
                x = [record.open,record.high,record.low,record.close,record.volume]
                y = record.adj_close
                #adj.append(y)

                s = [0,0,0,0,0]
                sTemp = [0,0,0,0,0]
                #Agrega los Valores a la Grafica
                #adj.append(y)

                for r in range(5):
                    for e in range(5):
                        s[e]= s[e] + float(tetas[r])*float(x[r])
                for z in range(5):
                    sTemp[z]=sTemp[z]+(s[z]-float(y))* float(x[z])
            for r in range(5):
                tetas[r] = tetas[r] - float(alpha[r]/size)* sTemp[r]
        print tetas

    def trainModelForSingleCompany(self,companyId):
        size = Record.objects.filter(company_id = companyId).count()
        allRecords = Record.objects.filter(company_id = companyId).all()
        tetas = [0,0,0,0,0]
        cont = 0
        alpha = [1000,1000,1000,1000,1000]
        utility_values = UtilityValues()
        utility_values.calculate(companyId)

        for i in range(50):
            for record in allRecords:
                cont = cont+1
                normalized_low = (record.low - utility_values.low.avg) / (utility_values.low.max - utility_values.low.min)
                normalized_high = (record.high - utility_values.high.avg) / (utility_values.high.max - utility_values.high.min)
                normalized_open = (record.open - utility_values.open.avg) / (utility_values.open.max - utility_values.open.min)
                normalized_volume = (record.volume - utility_values.volume.avg) / (utility_values.volume.max - utility_values.volume.min)
                normalized_close = (record.close - utility_values.close.avg) / (utility_values.close.max - utility_values.close.min)
                normalized_adj_close = (record.adj_close - utility_values.adj_close.avg) / (utility_values.adj_close.max - utility_values.adj_close.min)
                x = [normalized_open,normalized_high,normalized_low,normalized_close,normalized_volume]
                y = normalized_adj_close
                #adj.append(y)
                s = [0,0,0,0,0]
                sTemp = [0,0,0,0,0]
                #Agrega los Valores a la Grafica
                #adj.append(y)

                for r in range(5):
                    for e in range(5):
                        s[e]= s[e] + float(tetas[r])*float(x[r])
                for z in range(5):
                    sTemp[z]=sTemp[z]+(s[z]-float(y))* float(x[z])
            for r in range(5):
                tetas[r] = tetas[r] - float(alpha[r]/size)* sTemp[r]
        thetaValue = ThetaValue()
        thetaValue.datetime = datetime.datetime.now()
        thetaValue.company_id = companyId
        thetaValue.open_theta = tetas[0]
        thetaValue.high_theta = tetas[1]
        thetaValue.low_theta = tetas[2]
        thetaValue.close_theta = tetas[3]
        thetaValue.volume_theta = tetas[4]
        thetaValue.save()
        return thetaValue