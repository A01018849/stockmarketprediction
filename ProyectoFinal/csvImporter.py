import csv

import datetime

from ProyectoFinal.models import Company, Record


class CSVImporter:

    def importFromCSV(self,fileName):
        with open( fileName, "rb" ) as theFile:
            reader = csv.DictReader( theFile )
            for line in reader:
                initials = line['Symbol']
                #print line
                if Company.objects.filter(initials=initials).exists():
                    companyId = Company.objects.filter(initials=initials).first().id
                else:
                    company = Company()
                    company.initials = initials
                    company.name = line['Company']
                    company.save()
                    companyId = company.id
                record = Record()
                record.company_id = companyId
                record.open = line['Open']
                record.close = line['Close']
                record.high = line['High']
                record.low = line['Low']
                try:
                    record.date = datetime.datetime.strptime(line['Date'], '%d/%m/%Y').strftime('%Y-%m-%d')
                except ValueError:
                    record.date = datetime.datetime.strptime(line['Date'], '%d/%m/%y').strftime('%Y-%m-%d')
                record.volume = line['Volume']
                record.adj_close = line['Adj Close']
                record.save()

